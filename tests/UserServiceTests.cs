using NUnit.Framework;
using Microsoft.EntityFrameworkCore;
using System;

[TestFixture]
public class UserServiceTests
{
    private AppDbContext _dbContext = null!;
    private UserService _userService = null!;

    [SetUp]
    public void Setup()
    {
        var options = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
            .Options;
        _dbContext = new AppDbContext(options);
        _userService = new UserService(_dbContext);
    }

    [Test]
    public void CreateUser_ValidUser_ReturnsCreatedUser()
    {
        // Arrange
        var newUser = new User { Name = "John", Birthdate = DateTime.Now, Active = true };

        // Act
        if (_dbContext != null)
        {
            var createdUser = _userService.CreateUser(newUser);

            // Assert
            Assert.Equals(newUser.Birthdate, createdUser.Birthdate);
            Assert.Equals(newUser.Active, createdUser.Active);
        }
        else
        {
            Assert.Fail("_dbContext is null.");
        }
    }
}