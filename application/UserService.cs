using System;
using System.Collections.Generic;
using System.Linq;

public class UserService
{
    private readonly AppDbContext _dbContext;

    public UserService(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }

     public User CreateUser(User user)
    {
        var newUser = new User
        {
            Name = user.Name,
            Birthdate = user.Birthdate,
            Active = user.Active
        };

        _dbContext.User.Add(newUser);
        _dbContext.SaveChanges(); // Save changes for User

        return newUser;
    }

    public bool UpdateUserState(int userId, bool active)
    {
        var userToUpdate = _dbContext.User.FirstOrDefault(u => u.Id == userId);
        if (userToUpdate != null)
        {
            userToUpdate.Active = active;
            _dbContext.SaveChanges();  // Save changes for User
            return true;
        }
        return false;
    }

    public bool DeleteUser(int userId)
    {
        var userToDelete = _dbContext.User.FirstOrDefault(u => u.Id == userId);
        if (userToDelete != null)
        {
            _dbContext.User.Remove(userToDelete);
            _dbContext.SaveChanges();  // Save changes for User
            return true;
        }
        return false;
    }

    public IEnumerable<User> GetAllActiveUsers()
    {
        return _dbContext.User.Where(u => u.Active).ToList(); // Get list of all users
    }
}