using System.ComponentModel.DataAnnotations;

public class User
{
    public int Id { get; set; }

    [Required(ErrorMessage = "El nombre del usuario es obligatorio.")]
    public string? Name { get; set; }

    [Required(ErrorMessage = "La fecha de nacimiento del usuario es obligatoria.")]
    [DataType(DataType.Date)]
    public DateTime Birthdate { get; set; }

    [Required(ErrorMessage = "El estado de actividad del usuario es obligatorio.")]
    public bool Active { get; set; }
}