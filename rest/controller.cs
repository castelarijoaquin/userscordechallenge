using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

[ApiController]
[Route("[controller]")]
public class UserController : ControllerBase
{   
    // Check http://localhost:5072/swagger/index.html for more details about endpoints
    private readonly UserService _userService;

    public UserController(UserService userService)
    {
        _userService = userService;
    }

    [HttpPost]
    public IActionResult CreateUser(User newUser)
    {
        //endpoint POST http://localhost:5072/user with this body 
        //{"Name": "Joaquin Castelari","Birthdate": "1994-08-15","Active": true}

        var createdUser = _userService.CreateUser(newUser);
        return Created($"/user/{createdUser.Id}", createdUser);
    }

    [HttpPut("{id}/state")]
    public IActionResult UpdateUserState(int id, bool active)
    {
        //endpoint PUT http://localhost:5072/user/7/state?active=true
        var success = _userService.UpdateUserState(id, active);
        if (success)
        {
            return Ok();
        }
        return NotFound();
    }

    [HttpDelete("{id}")]
    public IActionResult DeleteUser(int id)
    {
        //endpoint DELETE http://localhost:5072/user/{id}
        var success = _userService.DeleteUser(id);
        if (success)
        {
            return Ok();
        }
        return NotFound();
    }

    [HttpGet("all")]
    public IActionResult GetAllActiveUsers()
    {
        //endpoint GET http://localhost:5072/user/all
        var users = _userService.GetAllActiveUsers();
        return Ok(users);
    }
}